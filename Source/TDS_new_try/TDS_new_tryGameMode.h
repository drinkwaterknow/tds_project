// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDS_new_tryGameMode.generated.h"

UCLASS(minimalapi)
class ATDS_new_tryGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDS_new_tryGameMode();
};



