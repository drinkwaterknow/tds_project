// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_new_try.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_new_try, "TDS_new_try" );

DEFINE_LOG_CATEGORY(LogTDS_new_try)
 