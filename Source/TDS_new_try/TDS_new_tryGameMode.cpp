// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_new_tryGameMode.h"
#include "TDS_new_tryPlayerController.h"
#include "TDS_new_tryCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDS_new_tryGameMode::ATDS_new_tryGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDS_new_tryPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}