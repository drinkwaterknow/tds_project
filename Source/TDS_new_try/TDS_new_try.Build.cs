// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDS_new_try : ModuleRules
{
	public TDS_new_try(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
    }
}
